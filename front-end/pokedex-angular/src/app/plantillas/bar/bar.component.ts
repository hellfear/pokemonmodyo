import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.css']
})

export class BarComponent implements OnInit {

  pokemonInput:String = ""; 
  constructor(private router:Router) { 
  }

  ngOnInit(): void {
  }

  getDetailsPokemon(){
    this.router.navigate(['detailspokemon',this.pokemonInput]);
  }
}
