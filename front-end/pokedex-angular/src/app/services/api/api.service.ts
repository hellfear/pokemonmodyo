import { Injectable } from '@angular/core';
import { listapokemonI } from '../../models/listapokemon.interface'
import { pokemondetailsI} from '../../models/pokemondetails.interface'
import { listapokemondetailsI} from '../../models/listapokemondetails.interface'
import { evolutionpokemonI} from '../../models/evolutionpokemon.interface'
import { HttpClient , HttpHeaders} from '@angular/common/http'
import { Observable} from 'rxjs'
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url:String = "http://34.219.131.27:8080/pokedex/"; 
  constructor(private http:HttpClient) { }

  getAllPokemons():Observable<listapokemonI[]>{
    let direccion = this.url + "listPokemons";
    return this.http.get<listapokemonI[]>(direccion);
  }

  getDetailsPokemonsApi(pokemonInput: string | null):Observable<pokemondetailsI>{
    let direccion = this.url + "listPokemons/" + pokemonInput;
    return this.http.get<pokemondetailsI>(direccion);
  }

  getAllDetailsPokemon(page: string | null):Observable<listapokemondetailsI[]>{
    let direccion = this.url + "listPokemonsDetails?page=" +  page;
    return this.http.get<listapokemondetailsI[]>(direccion);
  }

  getEvolutionsPokemon(name: string | null):Observable<evolutionpokemonI>{
    let direccion = this.url + "evolutionsPokemon/" +  name;
    return this.http.get<evolutionpokemonI>(direccion);
  }
}
