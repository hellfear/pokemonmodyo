import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { ApiService } from '../../services/api/api.service';
import { pokemondetailsI} from '../../models/pokemondetails.interface'


@Component({
  selector: 'app-detailspokemon',
  templateUrl: './detailspokemon.component.html',
  styleUrls: ['./detailspokemon.component.css']
})
export class DetailspokemonComponent implements OnInit {

  
  constructor(private activaterouter:ActivatedRoute, private router:Router,private api:ApiService) { }
  datosPokemon!: pokemondetailsI;
  labelpokemon:string | null ="";
  ngOnInit(): void {
    let namePokemon = this.activaterouter.snapshot.paramMap.get('pokemonInput');
    this.labelpokemon =namePokemon;
    this.api.getDetailsPokemonsApi(namePokemon).subscribe(data =>{
      this.datosPokemon = data;
    });
  }

 routeEvolutions():void{
   this.router.navigate(['evolutions/',this.labelpokemon]);
 }

}
