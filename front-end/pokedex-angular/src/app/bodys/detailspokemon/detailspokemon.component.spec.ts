import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailspokemonComponent } from './detailspokemon.component';

describe('DetailspokemonComponent', () => {
  let component: DetailspokemonComponent;
  let fixture: ComponentFixture<DetailspokemonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailspokemonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailspokemonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
