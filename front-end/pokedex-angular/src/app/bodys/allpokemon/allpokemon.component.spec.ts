import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllpokemonComponent } from './allpokemon.component';

describe('AllpokemonComponent', () => {
  let component: AllpokemonComponent;
  let fixture: ComponentFixture<AllpokemonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllpokemonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllpokemonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
