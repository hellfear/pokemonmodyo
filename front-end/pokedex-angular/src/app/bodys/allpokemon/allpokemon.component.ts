import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api/api.service';
import { Router, ActivatedRoute} from '@angular/router';
import { listapokemondetailsI} from '../../models/listapokemondetails.interface'

@Component({
  selector: 'app-allpokemon',
  templateUrl: './allpokemon.component.html',
  styleUrls: ['./allpokemon.component.css']
})
export class AllpokemonComponent implements OnInit {

  constructor(private activaterouter:ActivatedRoute, private router:Router,private api:ApiService) { }
  listaDatosPokemon!:listapokemondetailsI[];
  pagina:number=1;
  specificpage:number=1;
  ngOnInit(): void {
    let page = this.activaterouter.snapshot.paramMap.get('page');
    this.getAllPokemonDetails(page);
  }

  setPagePrevios():void{
    if (this.pagina>1) {
      this.pagina = this.pagina - 1;
    }
    this.getAllPokemonDetails(this.pagina.toString());
    this.router.navigate(['allpokemon',this.pagina.toString()]);  
  }

  setPageNext():void{
    if (this.pagina<=118) {
      this.pagina = this.pagina + 1;
    }
    this.getAllPokemonDetails(this.pagina.toString()); 
    this.router.navigate(['allpokemon',this.pagina.toString()]);
  }

  setSpecificPage():void{
    if (this.specificpage<=118 && this.specificpage>1) {
      this.pagina = this.specificpage;
    
    this.getAllPokemonDetails(this.pagina.toString()); 
    this.router.navigate(['allpokemon',this.pagina.toString()]);
    }
  }

  getAllPokemonDetails(page: string | null):void{
    this.api.getAllDetailsPokemon(page).subscribe(data =>{
      this.listaDatosPokemon = data;
    });
  }

  getSpecific(name:string){
    this.router.navigate(['detailspokemon',name]);
  }

}
