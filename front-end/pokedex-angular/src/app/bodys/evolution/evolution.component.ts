import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { ApiService } from '../../services/api/api.service';
import { evolutionpokemonI} from '../../models/evolutionpokemon.interface';
import { pokemondetailsI} from '../../models/pokemondetails.interface';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-evolution',
  templateUrl: './evolution.component.html',
  styleUrls: ['./evolution.component.css']
})
export class EvolutionComponent implements OnInit {

  constructor(private activaterouter:ActivatedRoute, private router:Router,private api:ApiService) { }
  evolutionpokemon!:evolutionpokemonI;
  firstp!:pokemondetailsI;
  secondp!:pokemondetailsI;
  thirdp!:pokemondetailsI;
  contevolutions:number =0;
  ngOnInit(): void {
    this.getEvolutionsApi();
  }

  getEvolutionsApi(): void{
    let name = this.activaterouter.snapshot.paramMap.get('name');
    this.api.getEvolutionsPokemon(name).subscribe(data =>{
      this.evolutionpokemon = data;
      
      this.api.getDetailsPokemonsApi(name).subscribe(data1 =>{
        this.firstp = data1;
        console.log(data1);
      });
      if(data.evolutions===null){
        console.log("Don't have evolutions");
      }else{
        this.contevolutions=data.evolutions.length;
        if(data.evolutions.length>0){
          this.api.getDetailsPokemonsApi(data.evolutions[0]).subscribe(data2 =>{
            this.secondp = data2;
            console.log(data2);
          });
        }
        if(data.evolutions.length>1){
          this.api.getDetailsPokemonsApi(data.evolutions[1]).subscribe(data3 =>{
            this.thirdp = data3;
            console.log(data3);
          });
        }
      }

    });
  }

}
