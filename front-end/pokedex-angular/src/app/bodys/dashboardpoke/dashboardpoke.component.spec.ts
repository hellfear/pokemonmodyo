import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardpokeComponent } from './dashboardpoke.component';

describe('DashboardpokeComponent', () => {
  let component: DashboardpokeComponent;
  let fixture: ComponentFixture<DashboardpokeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardpokeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardpokeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
