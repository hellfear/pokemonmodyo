import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboardpoke',
  templateUrl: './dashboardpoke.component.html',
  styleUrls: ['./dashboardpoke.component.css']
})
export class DashboardpokeComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  listAllPokemons(){
    this.router.navigate(['allpokemon',1])
  }

}
