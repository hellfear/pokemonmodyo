import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent } from './plantillas/header/header.component';
import { DashboardpokeComponent } from './bodys/dashboardpoke/dashboardpoke.component';
import { DetailspokemonComponent } from './bodys/detailspokemon/detailspokemon.component';
import { AllpokemonComponent } from './bodys/allpokemon/allpokemon.component';
import { EvolutionComponent } from './bodys/evolution/evolution.component';
import { BarComponent } from './plantillas/bar/bar.component';

const routes: Routes = [

  {path:'',redirectTo:'dashboardpoke',pathMatch:'full'},
  {path:'dashboardpoke',component:DashboardpokeComponent},
  {path:'detailspokemon/:pokemonInput',component:DetailspokemonComponent},
  {path:'allpokemon/:page',component:AllpokemonComponent},
  {path:'evolutions/:name',component:EvolutionComponent},
  {path:'bar',component:BarComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
