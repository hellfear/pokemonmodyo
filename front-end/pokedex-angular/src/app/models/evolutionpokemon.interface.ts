export interface evolutionpokemonI{
    name:string;
    evolutions: Array<string>;
    message:string;
}