export interface pokemondetailsI{
    id:string;
    abilities: Array<abilities>;
    types: Array<types>;
    weight:string;
    sprites:sprites;
    massage:string;
    evolutions:string;
}

export interface abilities{
    ability:ability;
    slot:string;
}

export interface ability{
    name:string;
}

export interface types{
    slot:string;
    type:type;
}
export interface type{
    name:string;
}
export interface sprites{
    back_female:string;
    back_shiny_female:string;
    back_default:string;
    front_female:string;
    front_shiny_female:string;
    back_shiny:string;
    front_default:string;
    front_shiny:string;
}