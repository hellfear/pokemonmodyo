import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './plantillas/header/header.component';
import { BarComponent } from './plantillas/bar/bar.component';
import { DashboardpokeComponent } from './bodys/dashboardpoke/dashboardpoke.component';
import { DetailspokemonComponent } from './bodys/detailspokemon/detailspokemon.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AllpokemonComponent } from './bodys/allpokemon/allpokemon.component';
import { EvolutionComponent } from './bodys/evolution/evolution.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BarComponent,
    DashboardpokeComponent,
    DetailspokemonComponent,
    AllpokemonComponent,
    EvolutionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
