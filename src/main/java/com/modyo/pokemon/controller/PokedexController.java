package com.modyo.pokemon.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.modyo.pokemon.model.DetailsPokemon;
import com.modyo.pokemon.model.EvolutionDetails;
import com.modyo.pokemon.model.ModelPokemon;
import com.modyo.pokemon.model.PokemonWithDetails;
import com.modyo.pokemon.model.ResourcePokemon;
import com.modyo.pokemon.service.EvolutionsPokemonService;
import com.modyo.pokemon.service.ResourceServicePokemon;

import utilities.Constant;

@RestController
@RequestMapping("/pokedex") 
public class PokedexController {
	
	@Autowired
	ResourceServicePokemon resourceServicePokemon;
	@Autowired
	EvolutionsPokemonService evolutionsPokemonService;
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/listPokemons")
	public List<ModelPokemon> getPokemons()  { 
		ResourcePokemon listPokemon = resourceServicePokemon.getResourcePokemon(Constant.ALL_POKEMON);
		return listPokemon.getResults();
    }
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/listPokemons/{namePokemon}")
	public DetailsPokemon getPokemonDetails(@PathVariable String namePokemon)  { 
		DetailsPokemon detailsPokemon = resourceServicePokemon.getPokemonDetails(namePokemon);
		return detailsPokemon;
    }
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/evolutionsPokemon/{namePokemon}")
	public EvolutionDetails getevolutionsPokemon(@PathVariable String namePokemon)  { 
		EvolutionDetails evolutionDetails = evolutionsPokemonService.getEvolutionPokemon(namePokemon);
		return evolutionDetails;
    }
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/listPokemonsDetails")
	public List<PokemonWithDetails> getPokemonsDetails(@RequestParam (value="page") int page)  { 
		List<PokemonWithDetails> listPokemonWithDetails = resourceServicePokemon.getPokemonsDetails(page);
		return listPokemonWithDetails;
    }
}
