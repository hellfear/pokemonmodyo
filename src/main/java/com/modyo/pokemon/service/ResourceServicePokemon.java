package com.modyo.pokemon.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.modyo.pokemon.model.DetailsPokemon;
import com.modyo.pokemon.model.ModelPokemon;
import com.modyo.pokemon.model.PokemonWithDetails;
import com.modyo.pokemon.model.ResourcePokemon;

import utilities.Constant;
import utilities.Headers;
import utilities.Urls;

@Component
public class ResourceServicePokemon {

	@Cacheable("pokedexCode")
	public ResourcePokemon getResourcePokemon(String pokedexCode) {
		RestTemplate plantilla = new RestTemplate();
		ResponseEntity<ResourcePokemon> Response = plantilla.exchange(Constant.URL_API + pokedexCode,
				HttpMethod.GET, Headers.getHeaders(), new ParameterizedTypeReference<ResourcePokemon>() {
				});
		ResourcePokemon resourcePokemon = Response.getBody();
		return resourcePokemon;
	}

	@Cacheable("namePokemon")
	public DetailsPokemon getPokemonDetails(String namePokemon) {
		RestTemplate plantilla = new RestTemplate();
		DetailsPokemon detailsPokemon = new DetailsPokemon() ;
		try {
			ResponseEntity<DetailsPokemon> response = plantilla.exchange(
					Constant.URL_API_DETAILS + namePokemon, HttpMethod.GET, Headers.getHeaders(),
					new ParameterizedTypeReference<DetailsPokemon>() {
					});
			detailsPokemon = response.getBody();
			String url = Urls.getCurrentUrl().replaceAll("listPokemons", "evolutionsPokemon");
			url = url.replaceAll("evolutionsPokemonDetails", "evolutionsPokemon/" +namePokemon );
			detailsPokemon.setEvolutions(url);
			detailsPokemon.setMassage(Constant.PROCESS_SUCCED);
		} catch (Exception e) {
			detailsPokemon.setMassage(Constant.DATA_NOT_FOUND);
			return detailsPokemon;
		}
		return detailsPokemon;
	}

	public List<PokemonWithDetails> getPokemonsDetails(int page) {
		ArrayList<PokemonWithDetails> listPokemonesDetails = new ArrayList<PokemonWithDetails>();
		page = page-1;
		page = page*10;
		ResourcePokemon resourcePokemon = getResourcePokemon(Constant.TEN_POKEMON + page);
		for (ModelPokemon namePokemon : resourcePokemon.getResults()) {
			PokemonWithDetails pokemonWithDetails = new PokemonWithDetails();
			pokemonWithDetails.setPokemon(namePokemon);
			pokemonWithDetails.setDetailsPokemon(getPokemonDetails(namePokemon.getName()));
			listPokemonesDetails.add(pokemonWithDetails);
		}

		return listPokemonesDetails;
	}

	@CacheEvict(value="pokedexCode",allEntries=true)
	  public void evictCache() {
		System.out.print("Evicting all entries from fooCache.");
	  }
	
	@CacheEvict(value="namePokemon",allEntries=true)
	  public void evictCacheName() {
	   System.out.print("Evicting all entries from fooCache.");
	  }
}
