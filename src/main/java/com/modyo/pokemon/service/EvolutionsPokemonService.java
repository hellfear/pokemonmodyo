package com.modyo.pokemon.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.modyo.pokemon.model.DetailsPokemon;
import com.modyo.pokemon.model.EvolutionDetails;
import com.modyo.pokemon.model.Evolves_to;
import com.modyo.pokemon.model.PokemonSpecies;
import com.modyo.pokemon.model.ResourceEvolutionChain;

import utilities.Constant;
import utilities.Headers;

@Component
public class EvolutionsPokemonService {

	@Autowired
	ResourceServicePokemon resourceServicePokemon;

	@Cacheable("namePokemon")
	public EvolutionDetails getEvolutionPokemon(String namePokemon) {
		DetailsPokemon detailsPokemon = resourceServicePokemon.getPokemonDetails(namePokemon);
		RestTemplate plantilla = new RestTemplate();
		PokemonSpecies pokemonSpecies = new PokemonSpecies();
		ResourceEvolutionChain resourceEvolutionChain = new ResourceEvolutionChain();
		EvolutionDetails evolutionDetails = new EvolutionDetails();
		try {
			ResponseEntity<PokemonSpecies> responsePokemonSpecies = plantilla.exchange(
					Constant.URL_API_EVOLUTION + detailsPokemon.getId(), HttpMethod.GET, Headers.getHeaders(),
					new ParameterizedTypeReference<PokemonSpecies>() {
					});
			pokemonSpecies = responsePokemonSpecies.getBody();
		} catch (Exception e) {
			evolutionDetails.setMessage(Constant.DATA_NOT_FOUND + " species");
		}
		if (evolutionDetails.getMessage() == null) {
			try {
				ResponseEntity<ResourceEvolutionChain> responseEvolutionChain = plantilla.exchange(
						pokemonSpecies.getEvolution_chain().getUrl(), HttpMethod.GET, Headers.getHeaders(),
						new ParameterizedTypeReference<ResourceEvolutionChain>() {
						});
				resourceEvolutionChain = responseEvolutionChain.getBody();
				
			} catch (Exception e) {
				evolutionDetails.setMessage(Constant.DATA_NOT_FOUND + pokemonSpecies.getEvolution_chain().getUrl() + e.getMessage());
			}
		}
		ArrayList<String> listEvolutions = new ArrayList<String>();
		if(!resourceEvolutionChain.getChain().getSpecies().getName().equals(namePokemon)){
			listEvolutions.add(resourceEvolutionChain.getChain().getSpecies().getName());
		}
		for(Evolves_to ListEvolutions  : resourceEvolutionChain.getChain().getEvolves_to()) {
			if(!ListEvolutions.getSpecies().getName().equals(namePokemon)){
				listEvolutions.add(ListEvolutions.getSpecies().getName());
			}
			if(!ListEvolutions.getEvolves_to().isEmpty()) {
				if(!ListEvolutions.getEvolves_to().get(0).getSpecies().getName().equals(namePokemon)){
					listEvolutions.add(ListEvolutions.getEvolves_to().get(0).getSpecies().getName());
				}
			}	
		}
		if(evolutionDetails.getMessage() == null){
			evolutionDetails.setName(namePokemon);
			if(listEvolutions.isEmpty()) {
				evolutionDetails.setMessage(Constant.DONT_HAVE_EVOLUTION);
			}else {
				evolutionDetails.setEvolutions(listEvolutions);
				evolutionDetails.setMessage(Constant.PROCESS_SUCCED);
			}
			
		}
		return evolutionDetails;
	}
	
	@CacheEvict(value="namePokemon",allEntries=true)
	  public void evictCache() {
		System.out.print("Evicting all entries from fooCache.");
	  }
}
