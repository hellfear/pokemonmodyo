package com.modyo.pokemon.model;

import java.util.List;

public class EvolutionDetails {
	private String name;
	private List<String> evolutions;
	private String message;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getEvolutions() {
		return evolutions;
	}
	public void setEvolutions(List<String> evolutions) {
		this.evolutions = evolutions;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
