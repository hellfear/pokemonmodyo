package com.modyo.pokemon.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PokemonWithDetails {
	private ModelPokemon pokemon;
	private DetailsPokemon detailsPokemon;
	
	public ModelPokemon getPokemon() {
		return pokemon;
	}
	public void setPokemon(ModelPokemon pokemon) {
		this.pokemon = pokemon;
	}
	public DetailsPokemon getDetailsPokemon() {
		return detailsPokemon;
	}
	public void setDetailsPokemon(DetailsPokemon detailsPokemon) {
		this.detailsPokemon = detailsPokemon;
	}
	
	
}
