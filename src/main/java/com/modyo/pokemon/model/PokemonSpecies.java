package com.modyo.pokemon.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PokemonSpecies {
	private Evolution_chain evolution_chain;

	public Evolution_chain getEvolution_chain() {
		return evolution_chain;
	}

	public void setEvolution_chain(Evolution_chain evolution_chain) {
		this.evolution_chain = evolution_chain;
	}	
}
