package com.modyo.pokemon.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Ability {

	private Ability__1 ability;
	private String is_hidden;
	private Integer slot;
	
	public Ability__1 getAbility() {
		return ability;
	}
	public void setAbility(Ability__1 ability) {
		this.ability = ability;
	}
	public String isIs_hidden() {
		return is_hidden;
	}
	public void setIs_hidden(String is_hidden) {
		this.is_hidden = is_hidden;
	}
	public Integer getSlot() {
		return slot;
	}
	public void setSlot(Integer slot) {
		this.slot = slot;
	}
}
