package utilities;

public class Constant {
	public static final String DATA_NOT_FOUND ="Data not found";
	public static final String PROCESS_SUCCED ="Process succeed";
	public static final String ALL_POKEMON ="pokemon?limit=1200&offset=0";
	public static final String URL_API ="https://pokeapi.co/api/v2/";
	public static final String URL_API_DETAILS =URL_API + "pokemon/";
	public static final String URL_API_EVOLUTION =URL_API +  "pokemon-species/";
	public static final String DONT_HAVE_EVOLUTION ="Don't have evolutions";
	public static final String TEN_POKEMON ="pokemon?limit=10&offset=";
}
