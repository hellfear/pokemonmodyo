package utilities;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


public class Urls {
	
	public static String  getCurrentUrl() {
		ServletUriComponentsBuilder methodUri = ServletUriComponentsBuilder.fromCurrentRequestUri();
		return methodUri.toUriString();
	}
}
